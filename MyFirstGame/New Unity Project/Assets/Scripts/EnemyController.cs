﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Tooltip("Bomba que se lanza")]
    public GameObject bomb;
    [Tooltip("Punto donde se generan las bombas")]
    public Transform spawnPosition;
    [Tooltip("Bombas por segundo")]
    public float bombRate = 0.5f;
    [Tooltip("Velocidad de caída de las bombas")]
    public float bombSpeed = 3.0f;
    [Tooltip("Velocidad de desplazamiento")]
    public float speed = 3.0f;
    [Tooltip("Mínima x durante el desplazamiento")]
    public float xMin = -10f;
    [Tooltip("Máxima x durante el desplazamiento")]
    public float xMax = 10f;

    private float currentDir;
    private float nextBombTime;

    // Start is called before the first frame update
    void Start()
    {
        if (bombRate <= 0.0)
        {
            Debug.LogWarning("bombRate debe ser positivo");
            bombRate = 1.0f;
        }
        currentDir = 1.0f;
        randomlyChangeDirection();
        nextBombTime = Time.time - 1.0f / bombRate;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 p = transform.position;
        p.x = p.x + (speed * currentDir * Time.deltaTime);
        if (p.x < xMin)
        {
            p.x = xMin;
            currentDir = currentDir * -1;
        }
        else if (p.x > xMax)
        {
            p.x = xMax;
            currentDir = currentDir * -1;
        }
        transform.position = p;

        if (Time.time >= nextBombTime)
        {
            Bombard();
            randomlyChangeDirection();
            nextBombTime = nextBombTime + 1.0f / bombRate;
        }
    }
    void Bombard()
    {
        if (bomb != null || spawnPosition != null)
        {
            GameObject newBomb;
            newBomb = Instantiate(bomb,spawnPosition.position,spawnPosition.rotation);
            BombController  bc;
            bc = newBomb.GetComponent<BombController>();
            bc.speed = bombSpeed;
        }
    }
    void randomlyChangeDirection()
    {
        if (Random.value < 0.5)
        {
            currentDir = currentDir * -1;
        }
    }
}
