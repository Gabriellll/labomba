﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombController : MonoBehaviour
{
    [Tooltip("Velocidad de caída de la bomba")]
    public float speed = 3;
    [Tooltip("Máxima Y permitida")]
    public float yLimit = 0.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Transform t = GetComponent<Transform>();
        Vector3 pos = t.position;
        pos.y = pos.y - speed * Time.deltaTime;
        t.position = pos;
        if (Grounded())
        {
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
            {
                BombController bc;
                bc = go.GetComponent<BombController>();
                if (bc != null)
                {
                    bc.Explode();
                }
            }
            Explode();
        }
    }
    bool Grounded()
    {
        return transform.position.y <= yLimit;
    }
    void Explode()
    {
        Destroy(this.gameObject);
    }
}
